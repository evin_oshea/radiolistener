package listener

import (
	"net/http"
	"log"
	"errors"
	"github.com/headzoo/surf"
)

const (
	// Youtube video meta source url
	URL_META = "http://www.youtube.com/get_video_info?&video_id="
	URL_WATCH = "http://www.youtube.com/watch?v="

func Browse(radio_id string) error {
	bow := surf.NewBrowser()
	// video, err := Get(radio_id)

	// While TRUE
	err := bow.Open(URL_WATCH+radio_id)
	if err != nil {
    panic(err)
		return
	}
}
