package listener

import (
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	// "os/exec"
	// "path/filepath"
	// "regexp"
	"strconv"
	"strings"
	"time"
)

// _________________________________________________________________

const (
	// Youtube video meta source url
	URL_META = "http://www.youtube.com/get_video_info?&video_id="
  URL = "https://r12---sn-bvvbax-cvne.googlevideo.com/videoplayback?cmbypass=yes&requiressl=yes&keepalive=yes&ei=QmDXW96vOYO48wTptYzIBg&noclen=1&source=yt_live_broadcast&ipbits=0&hang=1&gcr=us&sparams=cmbypass%2Ccompress%2Cei%2Cgcr%2Cgir%2Chang%2Cid%2Cip%2Cipbits%2Citag%2Ckeepalive%2Clive%2Cmime%2Cmm%2Cmn%2Cms%2Cmv%2Cnoclen%2Cpl%2Crequiressl%2Csource%2Cexpire&id=hHW1oY26kxQ.1&signature=E6ECE49E86C3A32F95124BC16C3D6950BC3D83.D63EECA512E6A9CA9A57A499664CA2895035F7D2&ms=lv&mt=1540840713&mv=u&expire=1540863139&pl=23&ip=71.234.250.22&compress=yes&mn=sn-bvvbax-cvne&gir=yes&live=1&c=WEB&itag=140&key=yt6&mime=audio%2Fmp4&mm=32&alr=yes&cpn=vUGi4FVUp_cpngcV&cver=2.20181025&sq=1175799&rn=2306&rbuf=33332"
)

const (
	KB float64 = 1 << (10 * (iota + 1))
	MB
	GB
)

var (
	// Video formats
	Formats = []string{"3gp", "mp4", "flv", "webm", "avi"}
)

// holds a video's information
type Video struct {
	Id, Title, Author, Keywords, Thumbnail_url string
	Avg_rating                                 float32
	View_count, Length_seconds                 int
	Formats                                    []Format
	Filename                                   string
}

type Format struct {
	Itag                     int
	Video_type, Quality, Url string
}

/**

type AudioFile struct {
	data []byte
	name string
	label string
}
**/


type Downloader interface {
	Download()
}

type FileStream struct {
	flux 		chan byte
	fileName 	string
}

func (s FileStream) FileName() string {
	return s.fileName
}

func Record(urls *chan string) {
	video_id := os.Getenv("RADIO_ID")
	for _, u := range urls {
		case <-u {
			url <- u

		}
	}
	// get the video id from the command line
	// MOVE THIS OUTSIDE OF HERE EVENTUALLY

	// fetch the video metadata
	video, err := Get(video_id)
	if err != nil {
		log.Println("ERROR: ", err)
		return
	}
	log.Println("Get got.")
	// parseURL(URL)

	printRadioMeta(video)
	chan dataURLs string
	err = downloadAudio(video, &dataURLS)
	parseAudioURL(&dataURLs)
	if err != nil {
		os.Exit(1)
	}
}

func downloadAudio(video Video, urls *chan string) error {
	// string url
	// for data in range(urls) {
	//   url <- data
		t := "mp4"
		now := time.Now().String()
		log.Printf("Downloading to '%s.%s'\n", video.Id+now, ext)

		filename := fmt.Sprintf("/radio/data/%s.%s", video.Id, ext)

		err := video.Download(filename, url)
		if err != nil {
			log.Println("Error:", err)
			log.Println("Unable to download video content from Youtube.")
		} else {
			log.Println("Downloaded video:", video.Filename)
		}
	// }
	return err
}

func parsePage(watchURL string) string {
	return nil
}

// _________________________________________________________________
// given a video id, get it's information from youtube
func Get(video_id string) (Video, error) {
	// fetch video meta from youtube
	query_string, err := fetchMeta(video_id)
	if err != nil {
		return Video{}, err
	}
	log.Println("About to Parse Meta.")

	meta, err := parseMeta(video_id, query_string)

	if err != nil {
		return Video{}, err
	}

	return *meta, nil
}

func (video *Video) Download(filename string, url string) []byte, error {
	var (
		err    error
		length int64
	)

	if err != nil {
		return fmt.Errorf("Unable to write to file %q: %s", err)
	}

	defer out.Close()

	video.Filename = filename

	// Get video content length
	if resp, err := http.Head(url); err != nil {
		return fmt.Errorf("Head request failed: %s", err)
	} else {
		if resp.StatusCode == 403 {
			return errors.New("Head request failed: Video is 403 forbidden")
		}

		if size := resp.Header.Get("Content-Length"); len(size) == 0 {
			return errors.New("Content-Length header is missing")
		} else if length, err = strconv.ParseInt(size, 10, 64); err != nil {
			return fmt.Errorf("Invalid Content-Length: %s", err)
		}
	}

	// Not using range requests by default, because Youtube is throttling
	// download speed. Using a single GET request for max speed.
	start := time.Now()
	resp, err := http.Get(url)
	if err != nil {
		return fmt.Errorf("Request failed: %s", err)
	}
	defer resp.Body.Close()

	body, err = io.util.Readall(resp.Body); err != nil {
		return err
	}

	log.Println(body)
	// AudioFile{data: body}
	// pg.Insert(AudioFile)
	return err
}
/**
// Returns video format index by Itag number, or nil if unknown
func (v *Video) IndexByItag(itag int) (int, *Format) {
	for i := range v.Formats {
		format := &v.Formats[i]
		if format.Itag == itag {
			return i, format
		}
	}
	return 0, nil
}
**/
// _________________________________________________________________
// fetch video meta from http
func fetchMeta(video_id string) (string, error) {
	resp, err := http.Get(URL_META + video_id)

	// fetch the meta information from http
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	query_string, _ := ioutil.ReadAll(resp.Body)

	return string(query_string), nil
}

// parse youtube video metadata and return a Video object
func parseMeta(video_id, query_string string) (*Video, error) {
	// parse the query string
	u, _ := url.Parse("?" + query_string)

	// parse url params
	query := u.Query()

	// no such video
	if query.Get("errorcode") != "" || query.Get("status") == "fail" {
		fmt.Println("errorcode in video info")
		return nil, errors.New(query.Get("reason"))
	}

	// collate the necessary params
	video := &Video{
		Id:            video_id,
		Title:         query.Get("title"),
		Author:        query.Get("author"),
		Keywords:      query.Get("keywords"),
		Thumbnail_url: query.Get("thumbnail_url"),
	}

	v, _ := strconv.Atoi(query.Get("view_count"))
	video.View_count = v

	r, _ := strconv.ParseFloat(query.Get("avg_rating"), 32)
	video.Avg_rating = float32(r)

	l, _ := strconv.Atoi(query.Get("length_seconds"))
	video.Length_seconds = l

	// further decode the format data
	format_params := strings.Split(query.Get("url_encoded_fmt_stream_map"), ",")

	// every video has multiple format choices. collate the list.
	for _, f := range format_params {
		furl, _ := url.Parse("?" + f)
		fquery := furl.Query()

		itag, _ := strconv.Atoi(fquery.Get("itag"))

		video.Formats = append(video.Formats, Format{
			Itag:       itag,
			Video_type: fquery.Get("type"),
			Quality:    fquery.Get("quality"),
			Url:        fquery.Get("url") + "&signature=" + fquery.Get("sig"),
		})
	}

	return video, nil
}


func printRadioMeta(video youtube.Video) {
	txt :=
		`
	ID	:	%s
	Title	:	%s
	Author	:	%s
	Views	:	%d
	Rating	:	%f`

	log.Printf(txt, video.Id, video.Title, video.Author, video.View_count, video.Avg_rating)
	log.Println("\n\n\tFormats")

	for i := 0; i < len(video.Formats); i++ {
		log.Printf("\t%d\tItag %d: %s\t%s\n", i, video.Formats[i].Itag, video.Formats[i].Quality, video.Formats[i].Video_type)
	}
	log.Println()
	log.Println()
}
