package listener

import (
	// "bufio"
	// "database/sql"
	// "fmt"
	// "os"
	// "strings"
	// "md5"
	// "sync"
	// _ "github.com/lib/pq"
)

type LabeledData interface{
	func Hash() int
	func Data() []byte
	func ToString() string
	func Label() string
	func Name() string
}

type AudioFile struct {
	data []byte
	name string
	label string
}

func (a AudioFile) Data() []byte {
	return a.data
}

func (a AudioFile) ToString() string {
	return MP4.DECODE(a.data)
}

func (a AudioFile) Label() string {
	return a.label
}

func (a AudioFile) Hash() int {
	return hash_file(a.ToString())
}

func hash_file(data string) {
  return md5(data)
}
