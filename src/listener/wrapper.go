package listener

import (
	// "bufio"
	// "database/sql"
	// "fmt"
	// "os"
	// "strings"
	// "sync"
	// _ "github.com/lib/pq"
)

type dbconn interface {
	Insert(uint32, int, int)
	Select() Video
}

type pgdb struct {
	db *sql.DB
}

func (pg pgdb) Insert(datum *LabeledData) {
	qry := 	"INSERT INTO TrackInfo VALUES (?, ?, ?)"
	err := db.Exec(qry, datum.Hash(), datum.Data(), datum.Label())
	return err
}

func (pg pgdb) Select() lineData {
	// iter, err := pg.db.Query(
	// 	"SELECT line_len, num_tokens, times_found FROM LineInfo",
	// )
	// defer iter.Close()
	// check(err)
	// var data lineData
	// var nc int
	// var nt int
	// var n int
	// for iter.Next() {
	// 	if err := iter.Scan(&nc, &nt, &n); err != nil {
	// 		check(err)
	// 	}
	// 	data = append(data, lineInfo{
	// 		numChars:  nc,
	// 		numTokens: nt,
	// 		count:     n})
	// }
	// return data
}

func CreateTables(conn dbconn) {
	pg, _ := conn.(pgdb)
	_, err := pg.db.Exec(
		`CREATE TABLE IF NOT EXISTS TrackInfo (PRIMARY KEY (file_hash),
																						track string,
						 																label string);`)
	return err
}
//
// func SetupMetaData(db *sql.DB) {
// 	// f, err := os.Open(os.Getenv("KEYWORD_FILE"))
// 	// check(err)
// 	// scanner := bufio.NewScanner(f)
// 	// var line string
// 	// qry := "INSERT INTO KeywordInfo (keyword, times_found) VALUES ($1, 0)"
// 	// for scanner.Scan() {
// 	// 	line = scanner.Text()
// 	// 	db.Exec(qry, strings.ToLower(line))
// 	// }
// }

func NewDBConn() dbconn { //cassdb {
	//return NewCassDB()
	return NewPGDB()
}

func NewPGDB() pgdb {
	session, err := sql.Open("postgres", fmt.Sprintf(
		"user=%s password=%s dbname=%s host=%s port=%s sslmode=disable",
		os.Getenv("USER"),
		os.Getenv("PASSWORD"),
		os.Getenv("DBNAME"),
		os.Getenv("HOST"),
		os.Getenv("DB_PORT"),
	))
	check(err)
	return pgdb{db: session}
}
