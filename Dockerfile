FROM golang:1.11.1
ADD ./src/listener /go/src/listener/
ADD ./src/vendor /go/src/
WORKDIR /go/src
ADD ./src/main.go /go/src/main.go

# RUN go install /go/src/listener
# # RUN install for FFMPEG -- goav
# RUN apt-get -y --force-yes install autoconf automake build-essential libass-dev libfreetype6-dev libsdl1.2-dev libtheora-dev libtool libva-dev libvdpau-dev libvorbis-dev libxcb1-dev libxcb-shm0-dev libxcb-xfixes0-dev pkg-config texi2html zlib1g-dev
# RUN apt-get install yasm
# RUN export FFMPEG_ROOT=$HOME/ffmpeg
# RUN export CGO_LDFLAGS="-L$FFMPEG_ROOT/lib/ -lavcodec -lavformat -lavutil -lswscale -lswresample -lavdevice -lavfilter"
# RUN export CGO_CFLAGS="-I$FFMPEG_ROOT/include"
# RUN export LD_LIBRARY_PATH=$HOME/ffmpeg/lib
